#ifndef IMAGE_DEP
#define IMAGE_DEP

#include <iostream>
#include <memory>
#include <string>
#include <vector>
#include <fstream>
#include <cmath>

namespace PNCJAK001
{
    typedef unsigned char u_char ;

    // ------------------------------------------------------------------------------
    // FILTER CLASS
    // ------------------------------------------------------------------------------
    class Filter
    {
        private:
            int N;
            float *weights;

        public:
            Filter(std::string filename);
            ~Filter(){ delete [] weights;}
            bool loadFilter(std::string filename);
            void examineFilter(Filter &f);
            float operator[](int index){return weights[index];}

        friend class Image;
    };

    // ------------------------------------------------------------------------------
    // IMAGE CLASS
    // ------------------------------------------------------------------------------
    class Image
    {
        private:
            int width, height;
            std::unique_ptr<u_char[]> data;

        public: 
            // ---- Constructors and Desctructors  
            Image(): width(0), height(0), data(nullptr){};  // Default
            Image(int w, int h, u_char* buffer);            // From buffer
            Image(int w, int h, std::vector<u_char> &buffer);            // From vector
            Image(std::string imagefile);                   // From PGM file
            Image(const Image &rhs);                        // Copy Constructor
            Image(Image &&rhs);                             // Move Constructor
            ~Image() = default;   

            // ---- Load and Save image
            bool load(std::string infile);
            bool save(std::string outfile);
            
            // ---- Accesors
            u_char* getImage(){ return data.get();}
            int getWidth(){return width;}
            int getHeight(){return height;}

            // ---- Darken Image
            void darken(int val);

            // ---- Operator Overloading
            Image &operator=(const Image &rhs);     // Copy assignment
            Image &operator=( Image &&rhs);         // Move assignment
            void operator!(void);                   // Invert
            Image & operator+=(Image& rhs);
            Image operator+(Image& rhs) const;      // Addition
            Image & operator-=(Image& rhs);
            Image operator-(Image& rhs) const;      // Subtraction
            Image & operator/(Image& rhs);          // Mask
            Image & operator*(int& f);              // Threshold   
            Image & operator%(Filter &f);           // Filter
            u_char operator[](int index);         // Accessor

            class iterator
            {
                friend class Image;
                private:
                    u_char* ptr;
                    // Private constructor - can be used in friend Image class
                    iterator(u_char* p): ptr(p){}

                public:
                    iterator(const iterator &rhs): ptr(rhs.ptr){}
                    iterator & operator=(const iterator & rhs);
                    bool operator!=(const iterator &rhs);
                    bool operator==(const iterator &rhs);
                    void operator++(void);
                    void operator--(void);
                    iterator & advance(iterator & end, int i);
                    iterator & retreat(iterator & beg, int i);
                    u_char& operator*(void);

            };
            
            iterator begin(void) { return iterator(data.get());}
            iterator end(void) { return iterator(data.get() + width*height );}

            // ---- Freinds (stream operations)
            friend std::ostream &operator <<(std::ostream& os,  Image &i);
            friend std::istream &operator >>(std::istream& os,  Image &i);
            friend class Filter;


    };

    // ---- I/O operators
    std::ostream &operator <<(std::ostream& os,  Image &i);
    std::istream &operator >>(std::istream& os,  Image &i);

    

}//end Namespace
#endif