# CSC3022H - Assignment 4 - Image Processing

## Project Structure 
Project root directory:

- `include/` - *all header files*

- `src/` - *source and object code*

    - `obj/` - *object code*

- `Makefile` - *rules to compile and link project*

    - `make` - rule to make the program executable

    - `make test` - rule to make the program's unit tests

- `images` - all input and output images (PGM format)

---

## How to 'make'
To build all object files required to run the `./imageops` executable, from project root directory type:

```
make
```

If testing is required rather use the command:

```
make test
```

One can then run the `./test` executable to test the program.

---

## Project Usage

```
./imageops <options> <outputfilename>
```

There are several options:

- `-a <image1> <image2>` 
    
    - adds image1 to image2

- `-s <image1> <image2>` 

    - subtracts image2 from image1

- `-i <image1> ` 

    - inverts image1 

- `-I <image1> <image2>` 

    - masks image1 with image2

- `-t <image> <f>` 

    - thresholds image1 with integer 

- `-f <image> <filter>` 

    - filters image with a filter kernel    



### An Example using most of the methods:

_Step 1:_ Create a mask:
```
./imageops -t shrek_rectangular.pgm 200 mask.pgm
```

_Step 2:_ Invert the mask:
```
./imageops -i mask.pgm mask_inv.pgm
```

_Step 3:_ Mask the original image with mask:
```
./imageops -I shrek_rectangular.pgm mask.pgm masked1.pgm
```

_Step 4:_ Mask the original image with the inverted mask:
```
./imageops -I shrek_rectangular.pgm mask_inv.pgm masked2.pgm
```

_Step 5:_ Add the two masked images:
```
./imageops -a masked1.pgm masked2.pgm final_output.pgm
```

Now the image ```final_output``` should be _identical_ to the original image.

---

### An example of filtering the image:
```
 ./imageops -f Lenna_standard.pgm motion_blur.fir testoutput.pgm
```

![Standard Lenna](./images/Lenna_standard.jpg "Standard Lenna")
![Blurred Lenna](./images/testoutput.jpg "Blurred Lenna")
---


