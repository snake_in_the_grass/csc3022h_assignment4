// --------------------------------------------------------------------------------
// CSC3022H - Assigment4 - Image Manipulation
// Jake Pencharz, April 2019
// 
// This is the driver program for the classes contained in the Image.cpp 
// implementation file.
// 
// --------------------------------------------------------------------------------

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <typeinfo>
#include <map>
#include "Image.h"

using namespace std;

// ---- Constants
string IMG_DIR = "./images/";
string FILT_DIR = "./filters/";

// ---- Function Prototypes
void addition(int argc, char **argv);
void subtraction(int argc, char **argv);
void inversion(int argc, char **argv);
void mask(int argc, char **argv);
void threshold(int argc, char **argv);
void filter(int argc, char **argv);

int main(int argc, char **argv)
{

    char option = argv[1][1];
    switch (option)
    {
    case 'a':
        cout << "Addition" << endl;
        addition(argc, argv);
        break;
    case 's':
        cout << "Subtraction" << endl;
        subtraction(argc, argv);
        break;
    case 'i':
        cout << "Inversion" << endl;
        inversion(argc, argv);
        break;
    case 'I':
        cout << "Mask" << endl;
        mask(argc, argv);
        break;
    case 't':
        cout << "Threshold" << endl;
        threshold(argc, argv);
        break;
    case 'f':
        cout << "Filtering" << endl;
        filter(argc, argv);
        break;    
    default:
        cout << "Error: " << argv[1] << " is not a valid option. Valid optins include: " << endl;
        cout << "-a:\tAddition" << endl;
        cout << "-s:\tSubtraction" << endl;
        cout << "-i:\tInversion" << endl;
        cout << "-I:\tMasking" << endl;
        cout << "-t:\tThresholding" << endl;
        cout << "-f:\tFiltering" << endl;

        return 1;
    }
    return 0;
}// end main

void addition(int argc, char **argv)
{
    if (argc < 5)
    {
        cout << "Not enough input arguments given\nRequired: <I1> <I2> <OutputFileName>" << endl;
        return;
    }

    cout << "Adding images " << argv[2] << " and " << argv[3] << endl;
    string file1 = IMG_DIR + argv[2], file2 = IMG_DIR + argv[3], outFile = IMG_DIR + argv[4];

    PNCJAK001::Image i(file1);
    PNCJAK001::Image j(file2);
    i = i + j;
    i.save(outFile);
}
void subtraction(int argc, char **argv)
{
    if (argc < 5)
    {
        cout << "Not enough input arguments given\nRequired: <I1> <I2> <OutputFileName>" << endl;
        return;
    }

    cout << "Subtracting images " << argv[3] << " from " << argv[2] << endl;
    string file1 = IMG_DIR + argv[2], file2 = IMG_DIR + argv[3], outFile = IMG_DIR + argv[4];

    PNCJAK001::Image i(file1);
    PNCJAK001::Image j(file2);
    i = i - j;
    i.save(outFile);
}
void inversion(int argc, char **argv)
{
    if (argc < 4)
    {
        cout << "Not enough input arguments given\nRequired: <I1> <I2> <OutputFileName>" << endl;
        return;
    }
    cout << "Inverting image " << argv[2] << endl;
    string file1 = IMG_DIR + argv[2], outFile = IMG_DIR + argv[3];

    PNCJAK001::Image i(file1);
    !i;
    i.save(outFile);
};
void mask(int argc, char **argv)
{
    if (argc < 5)
    {
        cout << "Not enough input arguments given\nRequired: <I1> <I2> <OutputFileName>" << endl;
        return;
    }

    cout << "Masking image " << argv[2] << " with " << argv[3] << endl;
    string file1 = IMG_DIR + argv[2], file2 = IMG_DIR + argv[3], outFile = IMG_DIR + argv[4];

    PNCJAK001::Image i(file1);
    PNCJAK001::Image j(file2);
    i/j;
    i.save(outFile);

};
void threshold(int argc, char **argv)
{
    if (argc < 5)
    {
        cout << "Not enough input arguments given\nRequired: <I1> <float> <OutputFileName>" << endl;
        return;
    }

    string file1 = IMG_DIR + argv[2], outFile = IMG_DIR + argv[4];
    int f;
    stringstream ss(argv[3]);
    ss >> f;
    cout << "Thresholding image " << argv[2] << " at " << f << endl;
    
    PNCJAK001::Image i(file1);
    i*(f);
    i.save(outFile);

};

void filter(int argc, char **argv)
{

    if (argc < 5)
    {
        cout << "Not enough input arguments given\nRequired: <I> <F> <OutputFileName>" << endl;
        return;
    }

    string imageFile = IMG_DIR + argv[2], outFile = IMG_DIR + argv[4];
    string filterFile = FILT_DIR + argv[3];
    cout << "Filtering image " << argv[2] << " with " << filterFile << endl;

    PNCJAK001::Filter f(filterFile);
    PNCJAK001::Image i(imageFile);
    i%(f);
    i.save(outFile);

}