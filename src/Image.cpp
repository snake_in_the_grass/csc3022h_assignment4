// --------------------------------------------------------------------------------
// CSC3022H - Assigment4 - Image Manipulation
// Jake Pencharz, April 2019
// 
// This implementation file contains:
// - Image class methods
// - Iterator class methods
// - Filter class methods
// 
// --------------------------------------------------------------------------------

#include "Image.h"


namespace PNCJAK001
{
    
// --------------------------------------------------------------------------------
// Image Methods
// --------------------------------------------------------------------------------    

// ---- Construct from a PMG file
Image::Image(std::string imagefile)
{
    if(!load(imagefile))
        std::cout << "Error reading image file " + imagefile << std::endl;
}

// ---- Construct from a buffer (for testing)
Image::Image(int w, int h, u_char* buffer): width(w), height(h)
{
    data.reset(buffer);
}
// ---- Construct from a vector (for testing)
Image::Image(int w, int h, std::vector<u_char> &buffer): width(w), height(h)
{   
    using namespace std;

    int n = buffer.size();
    u_char *temp = new u_char[n];
    int i = 0;
    for (std::vector<u_char>::iterator it = buffer.begin() ; it != buffer.end(); ++it)
    {
        temp[i] = *it;
        i++;
    }
    data.reset(temp);
}

// ---- Copy constructor
Image::Image(const Image &rhs): width(rhs.width), height(rhs.height)
{
    // ---- Resize data
    int n = width*height;
    u_char* space = new u_char[n];
    data.reset(space);
    
    // ---- Copy contents manually
    u_char* temp = rhs.data.get();
    for(int i=0; i < n; i++) 
        data.get()[i] = temp[i];
}

// ---- Move constructor
Image::Image( Image &&rhs): width(std::move(rhs.width)), height(std::move(rhs.height)), data(std::move(rhs.data))
{}

// ---- Copy assignemnt
Image& Image::operator=(const Image &rhs)
{
    if(this != &rhs)
    {
        width = rhs.width;
        height = rhs.height;

        // ---- Resize data
        int n = width*height;
        u_char* space = new u_char[n];
        data.reset(space);
        
        // ---- Copy contents manually
        u_char* temp = rhs.data.get();
        for(int i=0; i < n; i++) 
            data.get()[i] = temp[i];
    }
    return *this;
}

// ---- Move assignemnt
Image& Image::operator=( Image &&rhs)
{
    if(this != &rhs)
    {
        width = std::move(rhs.width);
        height = std::move(rhs.height);
        data = std::move(rhs.data);
    }
    return *this;   
}

bool Image::load(std::string infile)
{
    using namespace std;
    ifstream input(infile, ios::binary);
    if(!input)
    {
        cout << "Could not open file" << endl;
        return -1;
    }

    // ---- Dump unnecesary lines
    string dump, dimensions;
    int N;
    getline(input, dump);       
    while(input.peek() == '#') 
        getline(input, dump);
    
    // ---- Get dimensions
    input >> height >> width >> N >> ws;
    cout << "Dimensions of " + infile + " are " << height << "x" << width << endl;
    
    // ---- Read in a block
    int n = width*height;
    char * buffer = new char[n];
    input.read(buffer, n);
    data.reset((unsigned char *) buffer);

    return n == input.gcount();

}

bool Image::save(std::string outfile)
{
    using namespace std;
    // ---- check the image was extracted correctly
    ofstream output(outfile, ios::binary);
    if(output)
    {
        output << "P5\n" << height << " " << width << endl << 255 << endl;
        output.write( (char *) data.get() , width*height);
        return true;
    }
    return false;
}

void Image::darken(int val)
{
    Image::iterator beg = this->begin(), end = this->end();

    // u_char &c = *beg;
    while(beg != end)
    {   
        if(*beg > val)
            *beg = *beg - val;
        ++beg;
    }
}

// ---- Invert with ! operator
void Image::operator!(void)
{
    Image::iterator beg = this->begin(), end = this->end();

    // u_char &c = *beg;
    while(beg != end)
    {   
        *beg = 255-*beg;
        ++beg;
    }
}

// ---- Overload +=
Image & Image::operator+=(Image& rhs)
{
    int n = this->width *this->height ;
    if(n != rhs.width*rhs.height)
    {
        std::cout << "Images are not the same size - cannot add" << std::endl;
        return *this;
    }

    Image::iterator beg = this->begin(), end = this->end();
    Image::iterator r_beg = rhs.begin(), r_end = rhs.end();

    while(beg!=end)
    {
        if(int(*beg) + int(*r_beg) > 255)
            *beg = 255;
        else 
            *beg = *beg + *r_beg;
        
        ++beg; ++r_beg;
    }

    return *this;
}

// ---- Overload +
Image Image::operator+(Image& rhs) const
{
    Image result(*this);
    result+=rhs;
    return result;
}

// ---- Overload -=
Image & Image::operator-=(Image& rhs)
{
    int n = this->width *this->height ;
    if(n != rhs.width*rhs.height)
    {
        std::cout << "Images are not the same size - cannot subtract" << std::endl;
        return *this;
    }

    Image::iterator beg = this->begin(), end = this->end();
    Image::iterator r_beg = rhs.begin(), r_end = rhs.end();

    while(beg!=end)
    {
        if(int(*beg) - int(*r_beg) < 0)
            *beg = 0;
        else 
            *beg = *beg - *r_beg;
        
        ++beg; ++r_beg;
    }
    
    return *this;
}

// ---- Overload operator-
Image Image::operator-(Image& rhs) const
{
    Image result(*this);
    result-=rhs;
    return result;
}

// ---- Overload opertor/ for masking
Image & Image::operator/(Image& rhs)
{
    int n = this->width *this->height ;
    if(n != rhs.width*rhs.height)
    {
        std::cout << "Images are not the same size - cannot mask" << std::endl;
        return *this;
    }

    Image::iterator beg = this->begin(), end = this->end();
    Image::iterator r_beg = rhs.begin(), r_end = rhs.end();

    while(beg!=end)
    {
        if(*r_beg != 255)
            *beg = 0;            
        
        ++beg; ++r_beg;
    }

    return *this;
}

// ---- Overload * operator for thresholding
Image & Image::operator*(int& f)
{

    Image::iterator beg = this->begin(), end = this->end();

    while(beg!=end)
    {
        if(*beg < f)
            *beg = 0; 
        else           
            *beg = 255; 
        ++beg; 
    }

    return *this;

}

u_char Image::operator[](int index)
{
    u_char temp = 0;
    if(index < 0)
        return temp;
    else if(index >= width*height)
        return temp;
    else
        return data.get()[index];
}

// ---- Overload % operator for filtering
Image & Image::operator%(Filter & f)
{   
    using namespace std;

    int N = f.N, offset = N/2;
    cout << "Filter size " << N << " offset is " << offset <<  endl;
    float total = 0;
    u_char *temp = new u_char[width*height];


    for(int ii = 0; ii< width; ++ii)
    {
        for(int ij = 0; ij < height; ++ij)
        {
            total = 0;
            for(int fi = 0; fi < N; ++fi)
            {
                for(int fj = 0; fj < N; ++fj)
                {
                    // if(data.get()[width*(ii-offset+fi) + ij-offset+fj] == 0)
                    //     cout << ii << "; " << ij << " at (" << fi << "; " << fj << ")" << endl;
                    total += data.get()[width*(ii-offset+fi) + ij-offset+fj] * f[N*fi+fj];
                }
            }
            if(total < 0)
                total = 0;
            temp[width*ii + ij] = u_char(total); 
        }
    }

    data.reset(temp);
    return *this;
}

// --------------------------------------------------------------------------------
// Iterator Methods
// --------------------------------------------------------------------------------

// ---- Overload operator = (copy assigment)
Image::iterator & Image::iterator::operator=(const iterator & rhs)
{
    if(this != &rhs)
    {
        ptr = rhs.ptr;
    }
    return *this;
}
// ---- Overload operator ++
void  Image::iterator::operator++(void)
{
    ptr++;
}

// ---- Overload operator --
void  Image::iterator::operator--(void)
{
    ptr--;
}

// ---- Move iterator fowards by i
Image::iterator & Image::iterator::advance(iterator & end, int i)
{
    for(int j =0 ; j < i ; j ++)    
    {
        if(*this != end)
            ++*this;
    }
    return *this;
}
// ---- Move iterator backwards by i
Image::iterator & Image::iterator::retreat(iterator & beg, int i)
{
    for(int j =0 ; j < i ; j ++)    
    {
        if(*this != beg)
            --*this;
    }
    return *this;
}

// ---- Overload operator !=
bool Image::iterator::operator!=(const iterator &rhs)
{
    return ptr != rhs.ptr;
}
// ---- Overload operator !=
bool Image::iterator::operator==(const iterator &rhs)
{
    return ptr == rhs.ptr;
}
// ---- Operator * (dereference)
u_char& Image::iterator::operator*(void)
{
    return *ptr;
}



// --------------------------------------------------------------------------------
// Filter Class methods
// --------------------------------------------------------------------------------
Filter::Filter(std::string filename)
{
    if(!loadFilter(filename))
        std::cout << "Error reading filter file" << std::endl;
}

bool Filter::loadFilter(std::string filename)
{
    using namespace std;

    ifstream input(filename);
    if(!input)
    {
        cout << "Could not open filter file" << endl;
        return false;
    }
    
    // ---- Read in dimension followed by filter values
    input >> N;
    weights = new float[N*N];
    for(int i = 0; i < N*N; ++i)
        input >> weights[i];

    // examineFilter(*this);
    return true;
}

void Filter::examineFilter(Filter &f)
{
    for(int i = 0; i < N*N; ++i)
        std::cout << i << ": " << f[i] << std::endl;
}

// --------------------------------------------------------------------------------
// External Methods
// --------------------------------------------------------------------------------

std::ostream &operator <<(std::ostream& os, PNCJAK001::Image &i)
{
    Image::iterator beg = i.begin(), end = i.end();
    while(beg!=end)
    {
        os << *beg << " ";
        ++beg;
    }
    return os;
}

std::istream &operator >>(std::istream& is, PNCJAK001::Image &i)
{
    Image::iterator beg = i.begin(), end = i.end();

    u_char c;
    std::vector<u_char> temp;
    while(is >> c)
        temp.push_back(c);

    int n = temp.size()  ;      
    u_char *arr = new u_char[n];
    int index = 0;
    for (std::vector<u_char>::iterator it = temp.begin() ; it != temp.end(); ++it)
    {
        arr[index] = *it;
        index++;
    }
    i.data.reset(arr);
    i.width = sqrt(n);
    i.height = sqrt(n);

    return is;
}

}// end PNCJAK001