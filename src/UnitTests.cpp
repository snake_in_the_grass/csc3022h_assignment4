#include "catch.hpp"
#include "Image.h"
#include <sstream>
#include <iostream>
#include <vector>
#include <fstream>

using namespace std;
using namespace PNCJAK001;


TEST_CASE("CONSTRUCTORS", "[constructor]")
{
    unsigned char *buffer = new unsigned char[2];
    buffer[0] = 'a';
    buffer[1] = 'b';
    PNCJAK001::Image i(1, 2, buffer);

    REQUIRE(i.getImage()[0] == 'a');
    REQUIRE(i.getImage()[1] == 'b');

    SECTION("Copy constructors")
    {
        PNCJAK001::Image j(i);
        // Is the new image the same as the old one?
        REQUIRE(j.getImage()[0] == 'a');
        REQUIRE(j.getImage()[1] == 'b');
        // Is the old image unchanged?
        REQUIRE(i.getImage()[0] == 'a');
        REQUIRE(i.getImage()[1] == 'b');
    }
    SECTION("Move constructors")
    {
        PNCJAK001::Image k = std::move(i);
        // Is the new image the same as the old one?
        REQUIRE(k.getImage()[0] == 'a');
        REQUIRE(k.getImage()[1] == 'b');
        // Is the old image null?
        REQUIRE(i.getImage() == nullptr);
    }

    SECTION("Vector constructor")
    {
        vector<unsigned char> v, u;
        v.push_back('h');
        v.push_back('e');
        v.push_back('y');
        v.push_back('a');

        PNCJAK001::Image i(2,2,v);
        
        Image::iterator it_beg = i.begin();
        Image::iterator it_end = i.end();
        while (it_beg != it_end)
        {
            u.push_back(*it_beg);
            ++it_beg;
        }

        REQUIRE(u == v);

    }
}

TEST_CASE("ASSIGNMENT", "[assigment_operators]")
{
    unsigned char *buffer = new unsigned char[2];
    buffer[0] = 'a';
    buffer[1] = 'b';
    PNCJAK001::Image i(1, 2, buffer);

    SECTION("Copy assignemnt")
    {
        PNCJAK001::Image j;
        j = i;
        // Is the new image the same as the old one?
        REQUIRE(j.getImage()[0] == 'a');
        REQUIRE(j.getImage()[1] == 'b');
        // Is the old image unchanged?
        REQUIRE(i.getImage()[0] == 'a');
        REQUIRE(i.getImage()[1] == 'b');
    }

    SECTION("Move assignemnt")
    {
        PNCJAK001::Image j;
        j = std::move(i);
        // Is the new image the same as the old one?
        REQUIRE(j.getImage()[0] == 'a');
        REQUIRE(j.getImage()[1] == 'b');
        // Is the old null?
        REQUIRE(i.getImage() == nullptr);
    }
}

TEST_CASE("ACCESSOR", "accessor_operator")
{
    unsigned char *buffer = new unsigned char[4];
    buffer[0] = 'a';
    buffer[1] = 'b';
    buffer[2] = 'c';
    buffer[3] = 'd';
    PNCJAK001::Image i(2, 2, buffer);

    REQUIRE(i[3] == 'd');
    REQUIRE(i[-1] == 0);    // check boundary condition
    REQUIRE(i[4] == 0);     // check boundary condition
}

TEST_CASE("ITERATOR ACCESS", "[iterator]")
{
    unsigned char *buffer = new unsigned char[4];
    buffer[0] = 'a';
    buffer[1] = 'b';
    buffer[2] = 'c';
    buffer[3] = 'd';
    PNCJAK001::Image i(2, 2, buffer);

    Image::iterator it_beg = i.begin();
    Image::iterator it_end = i.end();

    SECTION("Iterate from start to end")
    {
        vector<u_char> u;
        u.push_back('a');
        u.push_back('b');
        u.push_back('c');
        u.push_back('d');
        vector<u_char> v;

        while (it_beg != it_end)
        {
            v.push_back(*it_beg);
            ++it_beg;
        }

        REQUIRE(u == v);
    }
    
    SECTION("Advance")
    {
        // Check advance
        it_beg.advance(it_end, 2);
        REQUIRE(*it_beg == 'c');

        // Make sure it cannot go out of bounds
        it_beg.advance(it_end, 10);
        REQUIRE(it_beg == it_end);
    }

    SECTION("Retreat")
    {
        Image::iterator const_beg = i.begin();
        // Check retreat
        it_beg.advance(it_end, 3);
        it_beg.retreat(const_beg, 2);
        REQUIRE(*it_beg == 'b');

        // Make sure it cannot go out of bounds
        it_beg.retreat(const_beg, 10);
        REQUIRE(it_beg == i.begin());
    }
}

TEST_CASE("DARKEN")
{
    unsigned char *buffer = new unsigned char[2];
    buffer[0] = 100;
    buffer[1] = 120;
    PNCJAK001::Image i(1, 2, buffer);

    vector<u_char> u;
    u.push_back(80);
    u.push_back(100);
    vector<u_char> v;

    i.darken(20);
    Image::iterator it_beg = i.begin();
    Image::iterator it_end = i.end();
    while (it_beg != it_end)
    {
        v.push_back(*it_beg);
        ++it_beg;
    }

    REQUIRE(u == v);
}

TEST_CASE("INVERT")
{
    unsigned char *buffer = new unsigned char[4];
    buffer[0] = 0;
    buffer[1] = 255;
    buffer[2] = 1;
    buffer[3] = 254;
    PNCJAK001::Image i(2, 2, buffer);

    vector<u_char> u;
    u.push_back(255);
    u.push_back(0);
    u.push_back(254);
    u.push_back(1);
    vector<u_char> v;

    !i;
    Image::iterator it_beg = i.begin();
    Image::iterator it_end = i.end();
    while (it_beg != it_end)
    {
        v.push_back(*it_beg);
        ++it_beg;
    }

    REQUIRE(u == v);
}

TEST_CASE("ADDITION")
{
    unsigned char *buffer1 = new unsigned char[4];
    buffer1[0] = 0;
    buffer1[1] = 10;
    buffer1[2] = 100;
    buffer1[3] = 255;
    PNCJAK001::Image i(2, 2, buffer1);
    unsigned char *buffer2 = new unsigned char[4];
    buffer2[0] = 1;
    buffer2[1] = 10;
    buffer2[2] = 105;
    buffer2[3] = 102;
    PNCJAK001::Image j(2, 2, buffer2);

    // Test that normal addition works and that the value cannot exceed 255
    vector<u_char> u;
    u.push_back(1);
    u.push_back(20);
    u.push_back(205);
    u.push_back(255);
    vector<u_char> v;

    SECTION("+=")
    {
        j += i;

        Image::iterator it_beg = j.begin();
        Image::iterator it_end = j.end();
        while (it_beg != it_end)
        {
            v.push_back(*it_beg);
            ++it_beg;
        }

        REQUIRE(u == v);
    }

    SECTION("+")
    {
        PNCJAK001::Image z;
        z = i + j;

        Image::iterator it_beg = z.begin();
        Image::iterator it_end = z.end();
        while (it_beg != it_end)
        {
            v.push_back(*it_beg);
            ++it_beg;
        }

        REQUIRE(u == v);
    }
}

TEST_CASE("SUBRTACTION")
{
    unsigned char *buffer1 = new unsigned char[4];
    buffer1[0] = 0;
    buffer1[1] = 10;
    buffer1[2] = 105;
    buffer1[3] = 100;
    PNCJAK001::Image i(2, 2, buffer1);
    unsigned char *buffer2 = new unsigned char[4];
    buffer2[0] = 1;
    buffer2[1] = 10;
    buffer2[2] = 100;
    buffer2[3] = 255;
    PNCJAK001::Image j(2, 2, buffer2);

    // Test that normal subtraction works and that the value cannot be lower than 0
    vector<u_char> u;
    u.push_back(1);
    u.push_back(0);
    u.push_back(0);
    u.push_back(155);
    vector<u_char> v;

    SECTION("-=")
    {
        j -= i;

        Image::iterator it_beg = j.begin();
        Image::iterator it_end = j.end();
        while (it_beg != it_end)
        {
            v.push_back(*it_beg);
            ++it_beg;
        }

        REQUIRE(u == v);
    }

    SECTION("-")
    {
        PNCJAK001::Image z;
        z = j-i;

        Image::iterator it_beg = z.begin();
        Image::iterator it_end = z.end();
        while (it_beg != it_end)
        {
            v.push_back(*it_beg);
            ++it_beg;
        }

        REQUIRE(u == v);
    }
}

TEST_CASE("MASKING")
{
    unsigned char *buffer1 = new unsigned char[4];
    buffer1[0] = 50;
    buffer1[1] = 10;
    buffer1[2] = 105;
    buffer1[3] = 100;
    PNCJAK001::Image i(2, 2, buffer1);
    unsigned char *buffer2 = new unsigned char[4];
    buffer2[0] = 0;
    buffer2[1] = 0;
    buffer2[2] = 100;
    buffer2[3] = 255;
    PNCJAK001::Image j(2, 2, buffer2);

    // Test that normal smakinsg occured for all non 255 values
    vector<u_char> u;
    u.push_back(0);
    u.push_back(0);
    u.push_back(0);
    u.push_back(100);
    vector<u_char> v;

    i/j;

    Image::iterator it_beg = i.begin();
    Image::iterator it_end = i.end();
    while (it_beg != it_end)
    {
        v.push_back(*it_beg);
        ++it_beg;
    }

    REQUIRE(u == v);


}
TEST_CASE("THRESHOLDING")
{
    unsigned char *buffer1 = new unsigned char[4];
    buffer1[0] = 50;
    buffer1[1] = 10;
    buffer1[2] = 105;
    buffer1[3] = 100;
    PNCJAK001::Image i(2, 2, buffer1);
    int threshold = 100;
    

    // Test that normal thresholding occured for all values below 100
    vector<u_char> u;
    u.push_back(0);
    u.push_back(0);
    u.push_back(255);
    u.push_back(255);
    vector<u_char> v;

    i*(threshold);

    Image::iterator it_beg = i.begin();
    Image::iterator it_end = i.end();
    while (it_beg != it_end)
    {
        v.push_back(*it_beg);
        ++it_beg;
    }

    REQUIRE(u == v);


}
TEST_CASE("OUTPUT OSTREAM <<")
{
    unsigned char *buffer1 = new unsigned char[4];
    buffer1[0] = 'h';
    buffer1[1] = 'e';
    buffer1[2] = 'y';
    buffer1[3] = 'a';
    PNCJAK001::Image i(2, 2, buffer1);

    ostringstream serial;
    serial << i;
    REQUIRE(serial.str() =="h e y a ");
}

TEST_CASE("INPUT OSTREAM >>")
{
    PNCJAK001::Image i;
    std::string s = "h e y a ";
    istringstream data(s);
    data  >> i;
    
    REQUIRE(i.getImage()[0] == 'h');
    REQUIRE(i.getImage()[1] == 'e');
    REQUIRE(i.getImage()[2] == 'y');
    REQUIRE(i.getImage()[3] == 'a');
    REQUIRE(i.getWidth() == 2);
    REQUIRE(i.getHeight() == 2);
}
