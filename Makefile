CC=g++
FLAGS=-std=c++11
SRC=./src
OBJ=./src/obj
INCLUDE_DIR=./include/
DEPS=$(INCLUDE_DIR)Image.h
TEST_DEPS=$(INCLUDE_DIR)catch.hpp

TEST_OBJECTS=$(addprefix $(OBJ)/, UnitTests.o UnitTestsMain.o Image.o)

OBJECTS=$(addprefix $(OBJ)/, \
	main.o \
	Image.o )

# Generate Exxecutable 
imageops: $(OBJECTS) $(DEPS)
	$(CC) $(FLAGS) $(OBJECTS) -I $(INCLUDE_DIR) -o $@ 


$(OBJ)/main.o:$(SRC)/main.cpp
	$(CC) -c -o $@ $< $(FLAGS) -I $(INCLUDE_DIR)

$(OBJ)/Image.o:$(SRC)/Image.cpp
	$(CC) -c -o $@ $< $(FLAGS) -I $(INCLUDE_DIR)

# Generate Test suite 
test: $(TEST_OBJECTS) $(TEST_DEPS)
	$(CC) $(FLAGS) $(TEST_OBJECTS) -I $(INCLUDE_DIR) -o $@ 

$(OBJ)/UnitTests.o:$(SRC)/UnitTests.cpp
	$(CC) -c -o $@ $< $(FLAGS) -I $(INCLUDE_DIR)

$(OBJ)/UnitTestsMain.o:$(SRC)/UnitTestsMain.cpp
	$(CC) -c -o $@ $< $(FLAGS) -I $(INCLUDE_DIR)		

clean:
	rm $(OBJ)/*.o 